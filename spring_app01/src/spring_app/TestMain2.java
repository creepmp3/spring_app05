package spring_app;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

public class TestMain2 {
	public static void main(String[] args) {
		// xml 파일의 내용을 읽어서 실행
		
		// 스프링 컨테이너
		BeanFactory factory = new XmlBeanFactory(new FileSystemResource("src/app.xml"));
		
		Object obj = factory.getBean("c");
		Hello k = (Hello)obj;
		
		k.sayHello("오덕");
	}
}
