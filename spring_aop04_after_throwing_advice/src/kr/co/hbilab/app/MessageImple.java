/**
 * 2015. 5. 28.
 */
package kr.co.hbilab.app;

/**
 * @author user
 *
 */
public class MessageImple implements Message{
    
    String data;
    
    
    
    public void setData(String data) {
        this.data = data;
    }

    @Override
    public void printMsg() {
        System.out.println("출력 메세지 : " + data);
    }

    @Override
    public void printThrowWxception() {
        throw new IllegalArgumentException();
    }
}
